package ru.t1.simanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
