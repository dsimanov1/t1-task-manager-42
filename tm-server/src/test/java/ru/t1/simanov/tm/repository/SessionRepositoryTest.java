package ru.t1.simanov.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    /*@NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private static String userId = "";
    @NotNull
    private final ISessionRepository repository = new SessionRepository(connection);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        repository.add(userId, USER_SESSION1);
        repository.add(userId, USER_SESSION2);
    }

    @After
    public void after() throws Exception {
        repository.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertNotNull(repository.add(userId, USER_SESSION3));
        @Nullable final Session session = repository.findOneById(userId, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
        Assert.assertEquals(userId, session.getUserId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Session> sessions = repository.findAll(userId);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final Session session = repository.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        repository.clear(userId);
        Assert.assertEquals(0, repository.getCount(userId));
    }

    @Test
    public void remove() throws Exception {
        @Nullable final Session removedSession = repository.remove(USER_SESSION2);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(USER_SESSION2.getId(), removedSession.getId());
        Assert.assertNull(repository.findOneById(removedSession.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @Nullable final Session removedSession = repository.remove(userId, USER_SESSION2);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(USER_SESSION2.getId(), removedSession.getId());
        Assert.assertNull(repository.findOneById(removedSession.getId()));
    }

    @Test
    public void getCountByUserId() throws Exception {
        Assert.assertEquals(2, repository.getCount(userId));
    }

    @Test
    public void removeAll() throws Exception {
        repository.removeAll(SESSION_LIST);
        Assert.assertEquals(0, repository.getCount(userId));
    }

    @Test
    public void update() throws Exception {
        USER_SESSION1.setRole(Role.ADMIN);
        repository.update(USER_SESSION1);
        Assert.assertEquals(Role.ADMIN, repository.findOneById(userId, USER_SESSION1.getId()).getRole());
    }*/

}
